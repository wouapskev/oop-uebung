package tuc.isse.uebung07;

import java.util.List;

public class Kasse {
	
	public WebShop shop;
	
	public Kasse(WebShop shop) {
		this.shop = shop;
		
	}

	public int cartSum(String customerName) {
		List<Integer> cart = shop.customer.cartItems.get(customerName);
		Integer sum = 0;
		// test
		for (Integer item : cart) {
			sum += shop.items.itemPrice.get(item);
		}
		return sum;
	}
	
	public Integer countItems(String customerName) {
		return shop.customer.cartItems.get(customerName).size();
	}
	
	public Integer countItems( String customerName, Integer itemID) {	
		Integer count = 0;
		for (Integer itemInCart : shop.customer.cartItems.get(customerName)) {
			if(itemID==itemInCart) {
				count++;
			}
		}
		return count;
	}

}
