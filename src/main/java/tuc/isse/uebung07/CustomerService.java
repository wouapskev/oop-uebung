package tuc.isse.uebung07;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class CustomerService {
	
	public List<String> customerNames = new Vector<String>(); 	
	public Map<String,List<Integer>> cartItems = new HashMap<String,List<Integer>>();
	
	
	public void addCustomer(String name) {
		customerNames.add(name);
		cartItems.put(name, new Vector<Integer>());		
	}
	

	@SuppressWarnings("unlikely-arg-type")
	public void removeCustomer(String name) {
			customerNames.remove(name);
			cartItems.remove(cartItems.get(name));
	}
	
	public void printCustomer() {
		for (String name : customerNames) {
			System.out.println("customer:" + name);
		}
	}
	
	public void addItemToCart(String name, int itemIndex) {
		cartItems.get(name).add(itemIndex);
	}

}