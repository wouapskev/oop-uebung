package tuc.isse.uebung07;

import java.util.List;
import java.util.Vector;

public class WebShop {	
	
	
	public Item items = new Item();
	public CustomerService customer = new CustomerService();
	public List<String> urls = new Vector<String>();
	
	public Integer addItem(String name, int price) {
		return items.addItem(name, price);
	}
	
	public void printItem(int itemIndex) {
		System.out.println("item " + itemIndex + ":" + items.itemNames.get(itemIndex) + " Price:" + items.itemPrice.get(itemIndex));
	}	

	public void addCustomer(String name) {
		customer.addCustomer(name);
	}	
	public void removeCustomer(String name) {
		  customer.removeCustomer(name);
	}
	
	public void printCustomer() {
		customer.printCustomer();
	}
	
	public void addItemToCart(String name, int itemIndex) {
		customer.addItemToCart(name, itemIndex);
	}
	
	public void addURL(String url) {
		urls.add(url);
	}
	
	public void removeURL(String url) {
		urls.remove(url);
	}
	
	public void printURL() {
		for (String url : urls) {
			System.out.println(url);
		}
	}
	
}
