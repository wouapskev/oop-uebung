package tuc.isse.uebung07;

import java.util.List;
import java.util.Vector;

public class Item {
	
	public List<String> itemNames = new Vector<String>();
	public List<Integer> itemPrice = new Vector<Integer>();
	
	
	public String getItemName(int itemIndex) {
		return itemNames.get(itemIndex);
	}
	
	public void setItemName(int itemIndex, String newName) {
		itemNames.set(itemIndex, newName);
	}
	
	public int getItemPrice(int itemIndex) {
		return itemPrice.get(itemIndex);
	}
	
	public void setItemPrice(int itemIndex, int newPrice) {
		itemPrice.set(itemIndex, newPrice);
	}
	
	public Integer addItem(String name, int price) {
		itemNames.add(name);
		itemPrice.add(price);
		return itemNames.size();
	}
	

}

